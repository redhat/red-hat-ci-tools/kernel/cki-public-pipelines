# External pipelines for public projects

## Usage

For a new project, create a new branch based on any non-main branch. This will
make it easier to follow projects you're interested in.
